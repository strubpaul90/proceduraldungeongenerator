using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour
{
    [SerializeField] public GameObject RoomPref;

    [SerializeField] public int MaxRoomSize;
    [SerializeField] public int MaxRoomSizeZ;
    public float roomSizeX;
    public float roomSizeY;
    public float roomSizeZ;

    public bool bossRoom;
    public Room spawnRoomObject;
    public bool spawnRoom;

    public Room[,,] roomArray;
    public bool[,,] isRoomArray;
    public bool[] CanDown;
    public bool[] CanUp;
    public int[][] SizeMultiplierZ;

    public static DungeonGenerator instance;

    private void Awake()
    {
        if (MaxRoomSizeZ <=1)
        {
            roomArray = new Room[MaxRoomSize * 2 + 3, MaxRoomSize * 2 + 3, 1];
            isRoomArray = new bool[MaxRoomSize * 2 + 3, MaxRoomSize * 2 + 3, 1];
        }
        else
        {
            roomArray = new Room[MaxRoomSize * 2 + 3, MaxRoomSize * 2 + 3, MaxRoomSizeZ * 2 + 3];
            isRoomArray = new bool[MaxRoomSize * 2 + 3, MaxRoomSize * 2 + 3, MaxRoomSizeZ * 2 + 3];
        }
        CanDown = new bool[MaxRoomSize * 2 + 3];
        CanUp = new bool[MaxRoomSize * 2 + 3];
        SizeMultiplierZ = new int[MaxRoomSize * 2 + 3][];
        for(int z = 0; z < SizeMultiplierZ.Length;z++)
        {
            SizeMultiplierZ[z] = new int[2];
            SizeMultiplierZ[z][0] = 0;
            SizeMultiplierZ[z][1] = 0;
        } 
        
        for (int x=0; x > MaxRoomSize * 2 + 3; x++)
        {
            for (int y = 0; y > MaxRoomSize * 2 + 3; y++)
            {
                for (int z = 0; z > MaxRoomSize * 2 + 3; z++)
                {
                    isRoomArray[x, y, z] = false;
                }
            }
        }
        if (DungeonGenerator.instance == null)
        {
            DungeonGenerator.instance = this;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        if (MaxRoomSizeZ <= 1)
        {
            SizeMultiplierZ[0][1] = 0;
            SizeMultiplierZ[0][0] = 0;
            spawnRoomObject.GetComponent<Room>().InitRoom(0, MaxRoomSize, MaxRoomSize, 0);
        }
        else
        {
            SizeMultiplierZ[MaxRoomSizeZ][1] = 0;
            SizeMultiplierZ[MaxRoomSizeZ][0] = 0;
            spawnRoomObject.GetComponent<Room>().InitRoom(0, MaxRoomSize, MaxRoomSize, MaxRoomSizeZ);
        }
    }
}
