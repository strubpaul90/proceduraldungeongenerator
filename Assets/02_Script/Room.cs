using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    bool isOpenLeft;
    bool isOpenRight;
    bool isOpenTop;
    bool isOpenBottom;
    bool isOpenUp;
    bool isOpenDown;

    int[] position = new int[3];

    [SerializeField] GameObject LeftWall;
    [SerializeField] GameObject RightWall;
    [SerializeField] GameObject TopWall;
    [SerializeField] GameObject BottomWall;
    [SerializeField] GameObject BossObject;
    [SerializeField] GameObject GroundUp;
    [SerializeField] GameObject GroundDown;
    [SerializeField] GameObject GroundSide;
    [SerializeField] GameObject GroundSide1;
    [SerializeField] GameObject GroundSide2;
    [SerializeField] GameObject GroundSide3;
    [SerializeField] GameObject GroundTop;
    [SerializeField] GameObject GroundTop1;
    [SerializeField] GameObject GroundTop2;
    [SerializeField] GameObject GroundTop3;

    public void InitRoom(int repetition,int x , int y, int z)
    {
        position[0] = x;
        position[1] = y;
        position[2] = z;
        DungeonGenerator.instance.isRoomArray[x, y, z] = true;
        DungeonGenerator.instance.roomArray[x, y, z] = this;
        if (repetition == 0)
        {
            DungeonGenerator.instance.spawnRoom = true;
            CreateRoomSpawn(repetition);
        }
        else if (repetition >= DungeonGenerator.instance.MaxRoomSize)
        {
            CreateRoom();
        }
        else if(isOpenDown || isOpenUp)
        {
            CreateRoomWhole(repetition);
        }
        else
        {
            CreateRoom(repetition);
        }
    }

    public void SetWholeUp()
    {
        Destroy(GroundUp);
        isOpenUp = true;
        GroundTop.SetActive(true);
        GroundTop1.SetActive(true);
        GroundTop2.SetActive(true);
        GroundTop3.SetActive(true);
    }

    public void SetWholeDown()
    {
        Destroy(GroundDown);
        isOpenDown = true;
        GroundSide.SetActive(true);
        GroundSide1.SetActive(true);
        GroundSide2.SetActive(true);
        GroundSide3.SetActive(true);
    }

    public void SetBossRoom()
    {
        DungeonGenerator.instance.bossRoom = true;
        BossObject.SetActive(true);
    }

    void CreateRoomSpawn(int repetition)
    {
        gameObject.transform.position = new Vector3(position[0] * DungeonGenerator.instance.roomSizeX, position[2] * DungeonGenerator.instance.roomSizeY, position[1] * DungeonGenerator.instance.roomSizeZ);
        CreateDoorBot();
        CreateDoorLeft();
        CreateDoorRight();
        CreateDoorTop();
        CreateNewRoom(position[0]+1, position[1],position[2], repetition);
        CreateNewRoom(position[0], position[1]+1, position[2], repetition);
        CreateNewRoom(position[0]-1, position[1], position[2], repetition);
        CreateNewRoom(position[0], position[1]-1, position[2], repetition);
    }

    void CreateNewRoom(int x , int y, int z,int repetition)
    {
        if (!DungeonGenerator.instance.isRoomArray[x, y, z])
        {
            
            GameObject roomToAdd = Instantiate(DungeonGenerator.instance.RoomPref, new Vector3((x + DungeonGenerator.instance.SizeMultiplierZ[z][0])* DungeonGenerator.instance.roomSizeX , z * DungeonGenerator.instance.roomSizeZ  ,( y + DungeonGenerator.instance.SizeMultiplierZ[z][1] ) * DungeonGenerator.instance.roomSizeY ), new Quaternion(0, 0, 0, 0), DungeonGenerator.instance.transform);
            roomToAdd.GetComponent<Room>().InitRoom(repetition + 1, x, y, z); 
            
        }
        
    }
    
    void CreateNewRoom(int x , int y, int z,bool up)
    {
        if (!DungeonGenerator.instance.isRoomArray[x, y, z])
        {
            if (up)
            {
                GameObject roomToAdd = Instantiate(DungeonGenerator.instance.RoomPref, new Vector3((x + DungeonGenerator.instance.SizeMultiplierZ[z-1][0]) * DungeonGenerator.instance.roomSizeX, z * DungeonGenerator.instance.roomSizeZ, (y + DungeonGenerator.instance.SizeMultiplierZ[z-1][1]) * DungeonGenerator.instance.roomSizeY), new Quaternion(0, 0, 0, 0), DungeonGenerator.instance.transform);
                DungeonGenerator.instance.SizeMultiplierZ[z][0] = (int)( (roomToAdd.transform.position.x / DungeonGenerator.instance.roomSizeX) - (DungeonGenerator.instance.spawnRoomObject.transform.position.x / DungeonGenerator.instance.roomSizeX));
                DungeonGenerator.instance.SizeMultiplierZ[z][1] = (int)((roomToAdd.transform.position.z / DungeonGenerator.instance.roomSizeY) - (DungeonGenerator.instance.spawnRoomObject.transform.position.z / DungeonGenerator.instance.roomSizeY));
                roomToAdd.GetComponent<Room>().SetWholeDown();
                roomToAdd.GetComponent<Room>().InitRoom(1, DungeonGenerator.instance.MaxRoomSize, DungeonGenerator.instance.MaxRoomSize, z);
            }
            else
            {
                GameObject roomToAdd = Instantiate(DungeonGenerator.instance.RoomPref, new Vector3((x + DungeonGenerator.instance.SizeMultiplierZ[z+1][0]) * DungeonGenerator.instance.roomSizeX, z * DungeonGenerator.instance.roomSizeZ, (y + DungeonGenerator.instance.SizeMultiplierZ[z+1][1]) * DungeonGenerator.instance.roomSizeY), new Quaternion(0, 0, 0, 0), DungeonGenerator.instance.transform);
                DungeonGenerator.instance.SizeMultiplierZ[z][0] = (int)((roomToAdd.transform.position.x / DungeonGenerator.instance.roomSizeX) - (DungeonGenerator.instance.spawnRoomObject.transform.position.x / DungeonGenerator.instance.roomSizeX));
                DungeonGenerator.instance.SizeMultiplierZ[z][1] = (int)((roomToAdd.transform.position.z / DungeonGenerator.instance.roomSizeY) - (DungeonGenerator.instance.spawnRoomObject.transform.position.z / DungeonGenerator.instance.roomSizeY));
                roomToAdd.GetComponent<Room>().SetWholeUp();
                roomToAdd.GetComponent<Room>().InitRoom(1, DungeonGenerator.instance.MaxRoomSize, DungeonGenerator.instance.MaxRoomSize, z);
            }
            //GameObject roomToAdd = Instantiate(DungeonGenerator.instance.RoomPref, new Vector3(x * DungeonGenerator.instance.roomSizeX + (DungeonGenerator.instance.roomSizeX * DungeonGenerator.instance.SizeMultiplierZ[z][0]), z * DungeonGenerator.instance.roomSizeZ  , y * DungeonGenerator.instance.roomSizeY + (DungeonGenerator.instance.roomSizeY * DungeonGenerator.instance.SizeMultiplierZ[z][1])), new Quaternion(0, 0, 0, 0), DungeonGenerator.instance.transform);
            //if (up) roomToAdd.GetComponent<Room>().SetWholeDown();
            //else roomToAdd.GetComponent<Room>().SetWholeUp();
            //roomToAdd.GetComponent<Room>().InitRoom(1, DungeonGenerator.instance.MaxRoomSize, DungeonGenerator.instance.MaxRoomSize, z); 
            
        }
        
    }

    void CreateRoomWhole(int repetition)
    {
        int left = LookForExitLeft();
        int right = LookForExitRight();
        int top = LookForExitTop();
        int bot = LookForExitBot();
        int count = 0;
        int side = 0;
        if (left == 1)
        {
            CreateDoorLeft();
            count++;
            side = 1;
        }
        else if (left == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorLeft();
                    //CreateNewRoom(position[0] - 1, position[1], position[2], repetition);
                    count++;
                    side = 1;
                    break;
                default:

                    break;
            }
        }
        if (top == 1)
        {
            CreateDoorTop();
            count++;
            side = 2;
        }
        else if (top == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorTop();
                    //CreateNewRoom(position[0], position[1]+1, position[2], repetition);
                    count++;
                    side = 2;
                    break;
                default:

                    break;
            }
        }
        if (right == 1)
        {
            CreateDoorRight();
            count++;
            side = 3;
        }
        else if (right == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorRight();
                    //CreateNewRoom(position[0] + 1, position[1], position[2], repetition);
                    count++;
                    side = 3;
                    break;
                default:

                    break;
            }
        }
        if (bot == 1)
        {
            CreateDoorBot();
            count++;
            side = 4;
        }
        else if (bot == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorBot();
                    //CreateNewRoom(position[0], position[1]-1, position[2], repetition);
                    count++;
                    side = 4;
                    break;
                default:

                    break;
            }
        }


        if (count == 1 && DungeonGenerator.instance.bossRoom && DungeonGenerator.instance.MaxRoomSizeZ > 1)
        {
            if (!DungeonGenerator.instance.CanUp[position[2]] && position[2] < (DungeonGenerator.instance.MaxRoomSizeZ * 2 + 2) && !isOpenUp && !isOpenDown)
            {
                DungeonGenerator.instance.CanUp[position[2]] = true;
                DungeonGenerator.instance.CanDown[position[2] + 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][1] = position[1];
                SetWholeUp();
                CreateNewRoom(position[0], position[1], position[2] + 1, true);
            }
            else if (!DungeonGenerator.instance.CanDown[position[2]] && position[2] > 0 && !isOpenUp && !isOpenDown)
            {
                DungeonGenerator.instance.CanDown[position[2]] = true;
                DungeonGenerator.instance.CanUp[position[2] - 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][1] = position[1];
                SetWholeDown();
                CreateNewRoom(position[0], position[1], position[2] - 1, false);
            }
        }
        if (count == 1 && !DungeonGenerator.instance.bossRoom)
        {
            DungeonGenerator.instance.roomArray[position[0], position[1], position[2]].SetBossRoom();
        }
        if (count == 0)
        {
            CreateDoorTop();
            CreateDoorRight();
            CreateDoorLeft();
            CreateDoorBot();
        }
        if (bot == 0 && isOpenBottom)
        {
            CreateNewRoom(position[0], position[1] - 1, position[2], repetition);
        }
        if (top == 0 && isOpenTop)
        {
            CreateNewRoom(position[0], position[1] + 1, position[2], repetition);
        }
        if (right == 0 && isOpenRight)
        {
            CreateNewRoom(position[0] + 1, position[1], position[2], repetition);
        }
        if (left == 0 && isOpenLeft)
        {
            CreateNewRoom(position[0] - 1, position[1], position[2], repetition);
        }
    }

    void CreateRoom()
    {
        int left = LookForExitLeft();
        int right = LookForExitRight();
        int top = LookForExitTop();
        int bot = LookForExitBot();
        int count = 0;
        int side = 0;
        if (left == 1)
        {
            CreateDoorLeft();
            count++;
            side = 1;
        }
        if (top == 1)
        {
            CreateDoorTop();
            count++;
            side = 2;
        }
        if (right == 1)
        {
            CreateDoorRight();
            count++;
            side = 3;
        }
        if (bot == 1)
        {
            CreateDoorBot();
            count++;
            side = 4;
        }
        if (count == 1 && DungeonGenerator.instance.bossRoom && DungeonGenerator.instance.MaxRoomSizeZ > 1)
        {
            if (!DungeonGenerator.instance.CanUp[position[2]] && position[2] < (DungeonGenerator.instance.MaxRoomSizeZ * 2 + 2))
            {
                DungeonGenerator.instance.CanUp[position[2]] = true;
                DungeonGenerator.instance.CanDown[position[2] + 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][1] = position[1];
                SetWholeUp();
                CreateNewRoom(position[0], position[1], position[2] + 1, true);
            }
            else if (!DungeonGenerator.instance.CanDown[position[2]] && position[2] > 0)
            {
                DungeonGenerator.instance.CanDown[position[2]] = true;
                DungeonGenerator.instance.CanUp[position[2] - 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][1] = position[1];
                SetWholeDown();
                CreateNewRoom(position[0], position[1], position[2] - 1, false);
            }
        }
        if (count == 1 && !DungeonGenerator.instance.bossRoom)
        {
            DungeonGenerator.instance.roomArray[position[0] , position[1], position[2]].SetBossRoom();
        }

    }

    void CreateRoom(int repetition)
    {
        int left = LookForExitLeft();
        int right = LookForExitRight();
        int top = LookForExitTop();
        int bot = LookForExitBot();
        int count = 0;
        int side = 0;
        if (left == 1)
        {
            CreateDoorLeft();
            count++;
            side = 1;
        }
        else if (left == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorLeft();
                    //CreateNewRoom(position[0] - 1, position[1], position[2], repetition);
                    count++;
                    side = 1;
                    break;
                default:

                    break;
            }
        }
        if (top == 1)
        {
            CreateDoorTop();
            count++;
            side = 2;
        }
        else if (top == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorTop();
                    //CreateNewRoom(position[0], position[1]+1, position[2], repetition);
                    count++;
                    side = 2;
                    break;
                default:

                    break;
            }
        }
        if (right == 1)
        {
            CreateDoorRight();
            count++;
            side = 3;
        }
        else if (right == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorRight();
                    //CreateNewRoom(position[0] + 1, position[1], position[2], repetition);
                    count++;
                    side = 3;
                    break;
                default:

                    break;
            }
        }
        if (bot == 1)
        {
            CreateDoorBot();
            count++;
            side = 4;
        }
        else if (bot == 0)
        {
            int r = Random.Range(0, 2);
            switch (r)
            {
                case 1:
                    CreateDoorBot();
                    //CreateNewRoom(position[0], position[1]-1, position[2], repetition);
                    count++;
                    side = 4;
                    break;
                default:

                    break;
            }
        }

        if (bot==0&&isOpenBottom)
        {
            CreateNewRoom(position[0], position[1] - 1, position[2], repetition);
        }
        if (top==0&&isOpenTop)
        {
            CreateNewRoom(position[0], position[1] + 1, position[2], repetition);
        }
        if (right==0&&isOpenRight)
        {
            CreateNewRoom(position[0] + 1, position[1], position[2], repetition);
        }
        if (left==0&&isOpenLeft)
        {
            CreateNewRoom(position[0] - 1, position[1], position[2], repetition);
        }
        if (count == 1 && DungeonGenerator.instance.bossRoom && DungeonGenerator.instance.MaxRoomSizeZ > 1)
        {
            if (!DungeonGenerator.instance.CanUp[position[2]] && position[2]<(DungeonGenerator.instance.MaxRoomSizeZ*2+2))
            {
                DungeonGenerator.instance.CanUp[position[2]] = true;
                DungeonGenerator.instance.CanDown[position[2] + 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] + 1][1] = position[1];
                SetWholeUp();
                CreateNewRoom(position[0], position[1], position[2] + 1, true);
            }
            else if (!DungeonGenerator.instance.CanDown[position[2]] && position[2] > 0)
            {
                DungeonGenerator.instance.CanDown[position[2]] = true;
                DungeonGenerator.instance.CanUp[position[2] - 1] = true;
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][0] = position[0];
                //DungeonGenerator.instance.SizeMultiplierZ[position[2] - 1][1] = position[1];
                SetWholeDown();
                CreateNewRoom(position[0], position[1], position[2] - 1, false);
            }
        }
        if (count == 1 && !DungeonGenerator.instance.bossRoom )
        {
            DungeonGenerator.instance.roomArray[position[0], position[1], position[2]].SetBossRoom();
        }

    }

    #region createDoor

    void CreateDoorRight()
    {
        isOpenRight = true;
        Destroy(RightWall);
    }
    
    void CreateDoorLeft()
    {
        isOpenLeft = true;
        Destroy(LeftWall);
    }
    
    void CreateDoorTop()
    {
        isOpenTop = true;
        Destroy(TopWall);
    }
    
    void CreateDoorBot()
    {
        isOpenBottom = true;
        Destroy(BottomWall);
    }

    #endregion


    #region lookForDoor

    int LookForExitRight()
    {
        if (DungeonGenerator.instance.isRoomArray[position[0] + 1, position[1],position[2]])
        {
            if (DungeonGenerator.instance.roomArray[position[0] + 1, position[1], position[2]].isOpenLeft)
            {
                return 1; //doit faire une chemin
            }
            else return 2; //ne doit pas faire de chemin
        }
        else
        {
            return 0; //pas de room a cot�
        }
    }

    int LookForExitTop()
    {
        if (DungeonGenerator.instance.isRoomArray[position[0] , position[1] + 1, position[2]])
        {
            if (DungeonGenerator.instance.roomArray[position[0] , position[1] + 1, position[2]].isOpenBottom)
            {
                return 1; //doit faire une chemin
            }
            else return 2; //ne doit pas faire de chemin
        }
        else
        {
            return 0; //pas de room a cot�
        }
    }

    int LookForExitBot()
    {
        if (DungeonGenerator.instance.isRoomArray[position[0], position[1] - 1, position[2]])
        {
            if (DungeonGenerator.instance.roomArray[position[0], position[1] - 1, position[2]].isOpenTop)
            {
                return 1; //doit faire une chemin
            }
            else return 2; //ne doit pas faire de chemin
        }
        else
        {
            return 0; //pas de room a cot�
        }
    }

    int LookForExitLeft()
    {
        if (DungeonGenerator.instance.isRoomArray[position[0] - 1, position[1], position[2]])
        {
            if (DungeonGenerator.instance.roomArray[position[0] - 1, position[1], position[2]].isOpenRight)
            {
                return 1; //doit faire une chemin
            }
            else return 2; //ne doit pas faire de chemin
        }
        else
        {
            return 0; //pas de room a cot�
        }
    }

    #endregion
}
